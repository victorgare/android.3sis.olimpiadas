package gare.com.br.olimpiadas;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private EditText edtNome;
    private EditText edtIdade;
    private Spinner spnModalide;
    private RadioGroup rdgPeriodo;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNome = (EditText) findViewById(R.id.edtNome);
        edtIdade = (EditText) findViewById(R.id.edtidade);
        rdgPeriodo = (RadioGroup) findViewById(R.id.rdgPeriodo);
        spnModalide = (Spinner) findViewById(R.id.spnModalidade);
    }

    public void reservar(View v) {
        dialog = new Dialog(this);

        dialog.setContentView(R.layout.dialog);

        EditText edtNomeD = (EditText) dialog.findViewById(R.id.edtNomeD);
        EditText edtIdadeD = (EditText) dialog.findViewById(R.id.edtIdadeD);
        EditText edtModadlidade = (EditText) dialog.findViewById(R.id.edtModaldidadeD);
        EditText edtPeriodo = (EditText) dialog.findViewById(R.id.edtPeriodoD);

        edtNomeD.setText(edtNome.getText().toString());
        edtIdadeD.setText(edtIdade.getText().toString());

        int idSelecionado = rdgPeriodo.getCheckedRadioButtonId();
        RadioButton rdbSelecionado = (RadioButton) findViewById(idSelecionado);

        edtPeriodo.setText(rdbSelecionado.getText().toString());

        edtModadlidade.setText(spnModalide.getSelectedItem().toString());


        dialog.show();
    }

    public void finalizar (View V){
        AlertDialog.Builder b = new AlertDialog.Builder(this);

        b.setTitle("Confirmação");

        b.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                encerrar();
            }
        });

        b.show();
    }
    public void encerrar() {
        dialog.dismiss();

        edtNome.setText(null);
        edtIdade.setText(null);
        spnModalide.setSelection(0);
        rdgPeriodo.check(R.id.rbManha);
    }
}
